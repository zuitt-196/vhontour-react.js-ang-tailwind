// import PAGES[SECTION]
import Home from './pages/Home'
import Profile from './pages/Profile'
import Offers from './pages/Offers'
import SignIn from './pages/Signin'
import SignUp from './pages/SignOf'
import ForgotPassword from './pages/ForgotPassword'

// import COMPONENTS[SECTION]
import Header from './components/Header'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import './App.css';

function App() {
  return (
    <>
      <Router>
        <Header />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/profile" element={<Profile />} />
          <Route path="/sign-in" element={<SignIn />} />
          <Route path="/sign-up" element={<SignUp />} />
          <Route path="/forgot-pasword" element={<ForgotPassword />} />
          <Route path="/offers" element={<Offers />} />
        </Routes>
      </Router>
    </>

  );
}

export default App;
